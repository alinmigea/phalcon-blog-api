<?php

namespace App\Exception;

class NotFoundException extends BaseException
{
    /** @inheritdoc */
    protected $code = 404;

    /** @inheritdoc  */
    protected $message = 'Not found.';
}
