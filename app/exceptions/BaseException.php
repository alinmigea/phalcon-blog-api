<?php

namespace App\Exception;

use Phalcon\Exception;

class BaseException extends Exception
{
    /** @inheritdoc */
    protected $code = 500;

    /** @inheritdoc */
    protected $message = 'Internal server error.';
}
