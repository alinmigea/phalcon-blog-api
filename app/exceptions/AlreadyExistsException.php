<?php

namespace App\Exception;

class AlreadyExistsException extends BaseException
{
    /** @inheritdoc */
    protected $code = 400;

    /** @inheritdoc */
    protected $message = 'Bad request.';
}
