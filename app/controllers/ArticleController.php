<?php

namespace App\Controller;

use App\Exception\BaseException;
use App\Helper\ResponseHelper;
use App\Service\ArticleService;
use App\Validation\ArticleValidation;
use Exception;
use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Model\Resultset;

class ArticleController extends Controller
{
    private ArticleService $service;

    private ArticleValidation $validation;

    private ResponseHelper $responseHelper;

    /**
     * Set the helpers on construct.
     *
     * @return void
     */
    public function onConstruct(): void
    {
        $this->service = $this->getDI()->getShared(ArticleService::class);
        $this->validation = $this->getDI()->getShared(ArticleValidation::class);
        $this->responseHelper = $this->getDI()->getShared(ResponseHelper::class);
    }

    /**
     * Get all articles.
     *
     * @return ResponseInterface
     */
    public function list(): ResponseInterface
    {
        /** @var Resultset $list */
        $list = $this->service->list();

        return $this->responseHelper->send($list);
    }

    /**
     * Get an article by id.
     *
     * @param int $id
     * @return ResponseInterface
     */
    public function view(int $id): ResponseInterface
    {
        try {
            $article = $this->service->view($id);
        } catch (Exception $exception) {
            return $this->responseHelper->send(
                $exception->getMessage(),
                ($exception instanceof BaseException) ? $exception->getCode() : 500
            );
        }

        return $this->responseHelper->send($article);
    }

    /**
     * Save a new article.
     *
     * @return ResponseInterface
     */
    public function add(): ResponseInterface
    {
        // validate the input
        $messages = $this->validation->validate($this->request->getJsonRawBody(true));
        if (count($messages)) {
            return $this->responseHelper->send($messages, 400);
        }

        try {
            $article = $this->service->add($this->request->getJsonRawBody(true));
        } catch (Exception $exception) {
            return $this->responseHelper->send(
                $exception->getMessage(),
                ($exception instanceof BaseException) ? $exception->getCode() : 500
            );
        }

        return $this->responseHelper->send($article, 201);
    }

    /**
     * Edit an existing article.
     *
     * @param int $id
     * @return ResponseInterface
     */
    public function edit(int $id): ResponseInterface
    {
        // validate the input
        $messages = $this->validation->validate($this->request->getJsonRawBody(true));
        if (count($messages)) {
            return $this->responseHelper->send($messages, 400);
        }

        try {
            $article = $this->service->edit($id, $this->request->getJsonRawBody(true));
        } catch (Exception $exception) {
            return $this->responseHelper->send(
                $exception->getMessage(),
                ($exception instanceof BaseException) ? $exception->getCode() : 500
            );
        }

        return $this->responseHelper->send($article);
    }

    /**
     * Delete an article.
     *
     * @param int $id
     * @return ResponseInterface
     */
    public function delete(int $id): ResponseInterface
    {
        try {
            $this->service->delete($id);
        } catch (Exception $exception) {
            return $this->responseHelper->send(
                $exception->getMessage(),
                ($exception instanceof BaseException) ? $exception->getCode() : 500
            );
        }

        return $this->responseHelper->send(null, 204);
    }

    /**
     * Get all categories for an article.
     *
     * @param int $id
     * @return ResponseInterface
     */
    public function categories(int $id): ResponseInterface
    {
        try {
            /** @var Resultset $list */
            $list = $this->service->listCategories($id);
        } catch (Exception $exception) {
            return $this->responseHelper->send(
                $exception->getMessage(),
                ($exception instanceof BaseException) ? $exception->getCode() : 500
            );
        }

        return $this->responseHelper->send($list);
    }
}
