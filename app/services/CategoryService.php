<?php

namespace App\Service;

use App\Exception\AlreadyExistsException;
use App\Exception\NotFoundException;
use App\Model\Article;
use App\Model\ArticleCategory;
use App\Model\Category;
use Exception;
use Phalcon\Di\Injectable;
use Phalcon\Mvc\Model\ResultsetInterface;
use Phalcon\Paginator\Adapter\Model;
use Phalcon\Paginator\RepositoryInterface;

class CategoryService extends Injectable
{
    /**
     * @return RepositoryInterface
     */
    public function list(): RepositoryInterface
    {
        $title = $this->request->get('title', ['string', 'trim']);
        if (strlen($title)) {
            $filters = [
                'conditions' => 'title LIKE :title:',
                'bind' => ['title' => '%' . $title . '%'],
            ];
        }

        $page = $this->request->get('page', 'int', 1);
        $limit = $this->request->get('limit', 'int', 10);
        $paginator = new Model([
            'model' => Category::class,
            'parameters' => $filters ?? [],
            'limit' => min($limit, 10),
            'page' => max($page, 1),
        ]);

        return $paginator->paginate();
    }

    /**
     * @param int $id
     * @return Category
     * @throws Exception
     */
    public function view(int $id): Category
    {
        return Category::findFirstById($id);
    }

    /**
     * @param array $data
     * @return Category
     */
    public function add(array $data): Category
    {
        $category = new Category();
        $category->assign($data);
        $category->save();

        return $category;
    }

    /**
     * @param int $id
     * @param array $data
     * @return Category
     * @throws Exception
     */
    public function edit(int $id, array $data): Category
    {
        $category = Category::findFirstById($id);
        $category->assign($data);
        $category->update();

        return $category;
    }

    /**
     * @param int $id
     * @return Category
     * @throws Exception
     */
    public function delete(int $id): Category
    {
        $category = Category::findFirstById($id);
        $category->delete();

        return $category;
    }

    /**
     * @param int $id
     * @return ResultsetInterface
     * @throws Exception
     */
    public function listArticles(int $id): ResultsetInterface
    {
        $category = Category::findFirstById($id);

        return $category->articles;
    }

    /**
     * @param int $id
     * @param int $articleId
     * @return void
     * @throws Exception
     */
    public function addArticle(int $id, int $articleId): void
    {
        $category = Category::findFirstById($id);
        $article = Article::findFirstById($articleId);

        if (!empty($category) && !empty($article)) {
            $articleCategory = new ArticleCategory();
            $articleCategory->article_id = $articleId;
            $articleCategory->category_id = $id;

            try {
                $articleCategory->save();
            } catch (Exception $exception) {
                throw new AlreadyExistsException('ArticleCategory already exists.');
            }
        }
    }

    /**
     * @param int $id
     * @param int $articleId
     * @return void
     * @throws Exception
     */
    public function deleteArticle(int $id, int $articleId): void
    {
        $category = Category::findFirstById($id);
        $article = Article::findFirstById($articleId);

        if (!empty($category) && !empty($article)) {
            $articleCategory = ArticleCategory::findFirst(
                [
                    'conditions' => 'article_id = :articleId: and category_id = :categoryId:',
                    'bind' => [
                        'articleId' => $article->id,
                        'categoryId' => $category->id
                    ]
                ]
            );

            if (!($articleCategory instanceof ArticleCategory)) {
                throw new NotFoundException('ArticleCategory not found.');
            }

            $articleCategory->delete();
        }
    }
}
