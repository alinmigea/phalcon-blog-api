<?php

namespace App\Service;

use App\Exception\AlreadyExistsException;
use App\Model\Article;
use App\Model\ArticleCategory;
use App\Model\Category;
use Exception;
use Phalcon\Di\Injectable;
use Phalcon\Mvc\Model\Resultset;
use Phalcon\Mvc\Model\ResultsetInterface;
use Phalcon\Paginator\Adapter\Model;
use Phalcon\Paginator\RepositoryInterface;

class ArticleService extends Injectable
{
    /**
     * @return RepositoryInterface
     */
    public function list(): RepositoryInterface
    {
        $title = $this->request->get('title', ['string', 'trim']);
        if (strlen($title)) {
            $filters = [
                'conditions' => 'title LIKE :title:',
                'bind' => ['title' => '%' . $title . '%'],
            ];
        }

        $page = $this->request->get('page', 'int', 1);
        $limit = $this->request->get('limit', 'int', 10);
        $paginator = new Model([
            'model' => Article::class,
            'parameters' => $filters ?? [],
            'limit' => min($limit, 10),
            'page' => max($page, 1),
        ]);

        return $paginator->paginate();
    }

    /**
     * @param int $id
     * @return Article
     * @throws Exception
     */
    public function view(int $id): Article
    {
        return Article::findFirstById($id);
    }

    /**
     * @param array $data
     * @return Article
     * @throws Exception
     */
    public function add(array $data): Article
    {
        // add the Article
        $article = new Article();
        $article->assign($data);
        $article->save();

        // add the Relations
        $this->addCategories($article->id, $data['categories']);

        return $article;
    }

    /**
     * @param int $id
     * @param array $data
     * @return Article
     * @throws Exception
     */
    public function edit(int $id, array $data): Article
    {
        // update the Article
        $article = Article::findFirstById($id);
        $article->assign($data);
        $article->update();

        // update the Relations
        $this->deleteCategories($article->id);
        $this->addCategories($article->id, $data['categories']);

        return $article;
    }

    /**
     * @param int $id
     * @return Article
     * @throws Exception
     */
    public function delete(int $id): Article
    {
        $article = Article::findFirstById($id);
        $article->delete();

        return $article;
    }

    /**
     * @param int $id
     * @return ResultsetInterface
     * @throws Exception
     */
    public function listCategories(int $id): ResultsetInterface
    {
        $article = Article::findFirstById($id);

        return $article->categories;
    }

    /**
     * @param int $id
     * @param int $categoryId
     */
    public function addCategory(int $id, int $categoryId): void
    {
        // todo
    }

    /**
     * @param int $id
     * @param int $categoryId
     */
    public function deleteCategory(int $id, int $categoryId): void
    {
        // todo
    }

    /**
     * @param int $articleId
     * @param array $categoryIds
     * @throws Exception
     */
    private function addCategories(int $articleId, array $categoryIds = []): void
    {
        $article = Article::findFirstById($articleId);

        $categories = [];
        foreach ($categoryIds as $categoryId) {
            $categories[] = Category::findFirstById($categoryId);
        }

        if (!empty($categories)) {
            $article->categories = $categories;
        }

        try {
            $article->update();
        } catch (Exception $exception) {
            throw new AlreadyExistsException('ArticleCategory already exists.');
        }
    }

    /**
     * @param int $articleId
     */
    private function deleteCategories(int $articleId): void
    {
        /** @var Resultset $articleCategories */
        $articleCategories = ArticleCategory::findByArticleId($articleId);
        if (count($articleCategories)) {
            $articleCategories->delete();
        }
    }
}
