<?php

namespace App\Service;

use App\Helper\FileHelper;
use App\Model\Author;
use Exception;
use Phalcon\Di\Injectable;
use Phalcon\Paginator\Adapter\Model;
use Phalcon\Paginator\RepositoryInterface;

class AuthorService extends Injectable
{
    /**
     * @return RepositoryInterface
     */
    public function list(): RepositoryInterface
    {
        $name = $this->request->get('name', ['string', 'trim']);
        if (strlen($name)) {
            $filters = [
                'conditions' => "CONCAT(first_name, ' ', last_name) LIKE :name:",
                'bind' => ['name' => '%' . $name . '%'],
            ];
        }

        $page = $this->request->get('page', 'int', 1);
        $limit = $this->request->get('limit', 'int', 10);
        $paginator = new Model([
            'model' => Author::class,
            'parameters' => $filters ?? [],
            'limit' => min($limit, 10),
            'page' => max($page, 1),
        ]);

        return $paginator->paginate();
    }

    /**
     * @param int $id
     * @return Author
     * @throws Exception
     */
    public function view(int $id): Author
    {
        return Author::findFirstById($id);
    }

    /**
     * @param array $data
     * @return Author
     */
    public function add(array $data): Author
    {
        $author = new Author();
        $data['image_path'] = $this->saveFile();
        $author->assign($data);
        $author->save();

        return $author;
    }

    /**
     * @param int $id
     * @param array $data
     * @return Author
     * @throws Exception
     */
    public function edit(int $id, array $data): Author
    {
        $author = Author::findFirstById($id);
        $data['image_path'] = $this->saveFile();
        $author->assign($data);
        $author->update();

        return $author;
    }

    /**
     * @param int $id
     * @return Author
     * @throws Exception
     */
    public function delete(int $id): Author
    {
        $author = Author::findFirstById($id);
        $author->delete();

        return $author;
    }

    /**
     * @return string|null
     */
    private function saveFile(): ?string
    {
        if ($this->request->hasFiles() == true) {
            $file = isset($this->request->getUploadedFiles()[0]) ? $this->request->getUploadedFiles()[0] : null;
            if (!empty($file)) {
                $fileHelper = new FileHelper($file);
                if ($fileHelper->save()) {
                    return $fileHelper->getName();
                }
            }
        }

        return null;
    }
}
