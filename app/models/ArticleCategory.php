<?php

namespace App\Model;

use App\Exception\NotFoundException;
use Exception;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\ResultsetInterface;

class ArticleCategory extends Model
{
    /**
     * @return void
     */
    public function initialize(): void
    {
        // ignore unknown columns
        Model::setup(['ignoreUnknownColumns' => true]);

        // define the table
        $this->setSource('articles_categories');

        // define the relationships
        $this->belongsTo(
            'article_id',
            Article::class,
            'id',
            [
                'alias' => 'article'
            ]
        );
        $this->belongsTo(
            'category_id',
            Category::class,
            'id',
            [
                'alias' => 'category'
            ]
        );
    }

    /**
     * @param int $id
     * @return ArticleCategory
     * @throws Exception
     */
    public static function findFirstById(int $id): ArticleCategory
    {
        /** @var ArticleCategory $article */
        $articleCategory = parent::findFirst($id);

        if (!($articleCategory instanceof ArticleCategory)) {
            throw new NotFoundException('ArticleCategory with id \'' . $id . '\' was not found.');
        }

        return $articleCategory;
    }

    /**
     * @param int $articleId
     * @return ResultsetInterface
     */
    public static function findByArticleId(int $articleId): ResultsetInterface
    {
        return parent::find([
            'conditions' => 'article_id = :articleId:',
            'bind' => array('articleId' => $articleId),
        ]);
    }
}
