<?php

namespace App\Model;

use App\Exception\NotFoundException;
use Carbon\Carbon;
use Exception;
use Phalcon\Mvc\Model;

class Category extends Model
{
    /**
     * @return void
     */
    public function initialize(): void
    {
        // ignore unknown columns
        Model::setup(['ignoreUnknownColumns' => true]);

        // define the table
        $this->setSource('categories');

        // define the relationships
        $this->hasMany(
            'id',
            ArticleCategory::class,
            'category_id',
            [
                'alias' => 'articlesCategories'
            ]
        );
        $this->hasManyToMany(
            'id',
            ArticleCategory::class,
            'category_id',
            'article_id',
            Article::class,
            'id',
            [
                'alias' => 'articles'
            ]
        );
    }

    /**
     * @param int $id
     * @return Category
     * @throws Exception
     */
    public static function findFirstById(int $id): Category
    {
        /** @var Category $article */
        $category = parent::findFirst($id);

        if (!($category instanceof Category)) {
            throw new NotFoundException('Category with id \'' . $id . '\' was not found.');
        }

        return $category;
    }

    /**
     * @return void
     */
    public function beforeUpdate(): void
    {
        $this->updated_at = Carbon::now()->format('Y-m-d');
    }
}
