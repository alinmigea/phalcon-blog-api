--
-- Table structure for table `categories`
--
CREATE TABLE `categories` (
  `id` INT(11) NOT NULL,
  `title` VARCHAR(256) COLLATE utf8_unicode_ci NOT NULL,
  `description` VARCHAR(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` DATETIME NOT NULL DEFAULT current_timestamp(),
  `updated_at` DATETIME NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` INT(11) NOT NULL AUTO_INCREMENT;

--
-- Table structure for table `authors`
--
CREATE TABLE `authors` (
  `id` INT(11) NOT NULL,
  `first_name` VARCHAR(128) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` VARCHAR(128) COLLATE utf8_unicode_ci NOT NULL,
  `email` VARCHAR(256) COLLATE utf8_unicode_ci NOT NULL,
  `image_path` VARCHAR(128) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for table `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`) USING BTREE;

--
-- AUTO_INCREMENT for table `authors`
--
ALTER TABLE `authors`
  MODIFY `id` INT(11) NOT NULL AUTO_INCREMENT;

--
-- Table structure for table `articles`
--
CREATE TABLE `articles` (
  `id` INT(11) NOT NULL,
  `title` VARCHAR(256) COLLATE utf8_unicode_ci NOT NULL,
  `description` VARCHAR(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author_id` INT(11) NOT NULL,
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  `updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `author_id` (`author_id`);

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` INT(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for table `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `fk_author` FOREIGN KEY (`author_id`) REFERENCES `authors` (`id`) ON DELETE NO ACTION;

--
-- Table structure for table `articles_categories`
--
CREATE TABLE `articles_categories` (
  `id` INT(11) NOT NULL,
  `article_id` INT(11) NOT NULL,
  `category_id` INT(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for table `articles_categories`
--
ALTER TABLE `articles_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `article_id_2` (`article_id`,`category_id`),
  ADD KEY `article_id` (`article_id`),
  ADD KEY `category_id` (`category_id`);

--
-- AUTO_INCREMENT for table `articles_categories`
--
ALTER TABLE `articles_categories`
  MODIFY `id` INT(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for table `articles_categories`
--
ALTER TABLE `articles_categories`
  ADD CONSTRAINT `article_id` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `category_id` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;
