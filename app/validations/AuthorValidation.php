<?php

namespace App\Validation;

use App\Model\Author;
use Exception;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Callback;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength\Max;

class AuthorValidation extends Validation
{
    public function initialize(): void
    {
        $this->add(
            'first_name',
            new PresenceOf(
                [
                    'message' => 'First name is required.',
                ]
            )
        );

        $this->add(
            'first_name',
            new Max(
                [
                    'message' => 'First name is too large. Max allowed 128 chars.',
                    'max' => 128,
                ]
            )
        );

        $this->add(
            'last_name',
            new PresenceOf(
                [
                    'message' => 'Last name is required.',
                ]
            )
        );

        $this->add(
            'last_name',
            new Max(
                [
                    'message' => 'Last name is too large. Max allowed 128 chars.',
                    'max' => 128,
                ]
            )
        );

        $this->add(
            'email',
            new PresenceOf(
                [
                    'message' => 'The e-mail is required.',
                ]
            )
        );

        $this->add(
            'email',
            new Email(
                [
                    'message' => 'The e-mail is not valid.',
                ]
            )
        );

        $this->add(
            'email',
            new Max(
                [
                    'message' => 'Email is too large. Max allowed 256 chars.',
                    'max' => 256,
                ]
            )
        );

        $this->add(
            'email',
            new Callback(
                [
                    'callback' => function ($data) {
                        if (isset($data['email'])) {
                            $conditions = 'email = :email:';
                            $bind['email'] = $data['email'];

                            if (isset($data['id'])) {
                                $conditions .= ' AND id <> :id:';
                                $bind['id'] = $data['id'];
                            }

                            try {
                                $exists = Author::findFirst([
                                    'conditions' => $conditions,
                                    'bind' => $bind
                                ]);

                                if (isset($exists) && $exists instanceof Author) {
                                    return false;
                                }
                            } catch (Exception $exception) {
                                return false;
                            }
                        }

                        return true;
                    },
                    'message' => 'Email already exists.',
                ]
            )
        );
    }
}
