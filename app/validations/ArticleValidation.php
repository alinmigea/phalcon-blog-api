<?php

namespace App\Validation;

use App\Model\Author;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Callback;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength\Max;

class ArticleValidation extends Validation
{
    public function initialize(): void
    {
        $this->add(
            'title',
            new PresenceOf(
                [
                    'message' => 'Title is required.',
                ]
            )
        );

        $this->add(
            'title',
            new Max(
                [
                    'message' => 'Title is too large. Max allowed 256 chars.',
                    'max' => 256,
                ]
            )
        );

        $this->add(
            'description',
            new Max(
                [
                    'message' => 'Description is too large. Max allowed 512 chars.',
                    'max' => 512,
                    'allowEmpty' => true,
                ]
            )
        );

        $this->add(
            'author_id',
            new PresenceOf(
                [
                    'message' => 'Author id is required.',
                    'cancelOnFail' => true
                ]
            )
        );

        $this->add(
            'author_id',
            new Callback(
                [
                    'callback' => function ($data) {
                        if (isset($data['author_id'])) {
                            try {
                                $exists = Author::findFirstById($data['author_id']);
                                if (isset($exists) && $exists instanceof Author) {
                                    return true;
                                }
                            } catch (\Exception $exception) {
                                return false;
                            }
                        }

                        return false;
                    },
                    'message' => 'Author doesn\'t exist.',
                ]
            )
        );
    }
}
