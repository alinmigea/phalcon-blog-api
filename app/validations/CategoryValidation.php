<?php

namespace App\Validation;

use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength\Max;

class CategoryValidation extends Validation
{
    public function initialize(): void
    {
        $this->add(
            'title',
            new PresenceOf(
                [
                    'message' => 'Title is required.',
                ]
            )
        );

        $this->add(
            'title',
            new Max(
                [
                    'message' => 'Title is too large. Max allowed 256 chars.',
                    'max' => 256,
                ]
            )
        );

        $this->add(
            'description',
            new Max(
                [
                    'message' => 'Description is too large. Max allowed 512 chars.',
                    'max' => 512,
                    'allowEmpty' => true,
                ]
            )
        );
    }
}
