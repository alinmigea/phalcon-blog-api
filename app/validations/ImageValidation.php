<?php

namespace App\Validation;

use Phalcon\Validation;
use Phalcon\Validation\Validator\File;

class ImageValidation extends Validation
{
    public function initialize(): void
    {
        $config = $this->getDI()->getShared('config')->path('upload');

        $this->add(
            'image',
            new File(
                [
                    'maxSize' => $config->maxSize,
                    'messageSize' => ':field exceeds the max filesize (:max).',
                    'allowedTypes' =>  $config->allowedTypes->toArray(),
                    'messageType' => 'Allowed file types are :types.',
                    'maxResolution' => $config->maxResolution,
                    'messageMaxResolution' => 'Max resolution of :field is :max.',
                ]
            )
        );
    }
}
