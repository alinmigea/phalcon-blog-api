<?php

namespace App\Helper;

use Phalcon\Di\Injectable;
use Phalcon\Http\Request\FileInterface;

class FileHelper extends Injectable
{
    private FileInterface $file;

    private string $name;

    private string $path;

    /**
     * FileHelper constructor.
     *
     * @param FileInterface $file
     */
    public function __construct(FileInterface $file)
    {
        $this->file = $file;
        $this->path = $this->config->path('upload.dirPath');
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function save(): bool
    {
        if ($this->isValid()) {
            do {
                $this->generateName();
            } while ($this->exists());

            return $this->move();
        }

        return false;
    }

    /**
     * @return bool
     */
    private function move(): bool
    {
        $target = $this->path . '/' . $this->name;

        return $this->file->moveTo($target);
    }

    /**
     * @return bool
     */
    private function exists(): bool
    {
        if (file_exists($this->path . '/' . $this->name)) {
            return true;
        }

        return false;
    }

    /**
     * @return void
     */
    private function generateName(): void
    {
        $pathInfo = pathinfo($this->file->getName());
        if (isset($pathInfo['filename'])) {
            $this->name = $pathInfo['filename']
                . '_' . round(microtime(true) * 1000)
                . '.' . $this->file->getExtension();
        }

        $this->name = str_replace(' ', '_', strtolower($this->name));
    }

    /**
     * @return bool
     */
    private function isValid(): bool
    {
        if (!empty($this->file->getName()) && !empty($this->file->getExtension())) {
            return true;
        }

        return false;
    }
}
