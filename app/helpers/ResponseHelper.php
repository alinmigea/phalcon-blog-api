<?php

namespace App\Helper;

use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\Controller;

class ResponseHelper extends Controller
{
    /**
     * @param mixed $content
     * @param int $code
     * @return ResponseInterface
     */
    public function send($content, int $code = 200): ResponseInterface
    {
        $this->response->setStatusCode($code);
        if (isset($content)) {
            $this->response->setJsonContent($content);
        }

        return $this->response->send();
    }
}
