
# Planning

## Day 1

Setup Windows environment for PHP development. Installed XAMPP. Installed PHPStorm. Installed MySQL Workbench. Installed Postman.

## Day 2

Setup environment for PHP development with Phalcon. Installed psr and phalcon DLL's.

## Day 3

Create a dummy routing file to get some basic requests for the Phalcon Blog API project.

## Day 4

Created the database for Phalcon Blog API project. Created the project architecture based on MVC architecture.

## Day 5

Continued on updating the database tables for Phalcon Blog API project.
Added routes for the Article, Author and Category items from the Database.

## Day 6

Finished adding all the exiting routes.
Added the cross routes to identify which articles are assign to which category.

## Day 7

Finished first version of the project. This was made with simple PDO connection.
Start creating the models in order to be able to easily validate the input data.

## Day 8

Added the Article, Category and Author models which extends base Model class.
Injected the Database Service Provider, in order to connect to the MySQL database.
Refactored the requests made to the Controllers.

## Day 9

Implemented the Services for Article, Category and Author requests.
Implemented the requests to retrieve related Articles or related Categories.
Refactored the requests made to the Controllers.

## Day 10

Implemented the Validations for Article, Category and Author models.

## Day 11

Received feedback from Carmen.
Started working on the feedback received.

## Day 12

Installed Code Sniffer and Beautifier in order to comply to PSR-12.
Worked on the feedback received from Carmen, by implementing the Providers for Database, Routes, Models, Services and Validations.

## Day 13

Worked on the Config Provider in order to load multiple configs from the config folder.

## Day 14

Fixed the use of Config Provider in Image Validation class.
Worked on refactoring the Service Providers in order to use assign method.
Worked on the Controllers in order to return proper Response class object.

## Day 15

Worked on displaying the associated categories for an article.
Worked on adding a category ids, directly when adding or editing an article.

## Day 16

Implemented the catching of exceptions when searching/saving/editing/deleting an article.
Added Swagger as a documentation generator for the project.

## Day 17

Worked on the review received from the colleagues. 
Started working on using the DotEnv library for injecting variables into Environment Variables.
Worked on refactoring the image processing into a FileHelper class.

## Day 18

Worked on using jsonSerialize method for creating a custom response. 
Implemented the beforeUpdate methods for Article and Category.

## Day 19

Added pagination for when retrieving the list of Articles, Categories or Authors.
Created custom exception classes for use instead of the classic Exception class.
Made some refactoring on how to check for a given parameter.

## Day 20

Created a response helper in order to parse the returned response from every controller.
Added an email validation, which was missing in the previous commits.
Made a presentation for my colleagues with my accommodation project.

## Day 21

@todo